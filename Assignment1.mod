/*********************************************
 * OPL 12.6.3.0 Model
 * Author: Thomas Mulder, Arjan van den Boogaart
 * Creation Date: 21 sep. 2016 at 12:06:54
 *********************************************/

 using CP;

//Tuple definitions
tuple Character { //A character has a distinct name and a type
	key string name;
	string type;
}

tuple Scene { //A scene has a name and a set of character names
	string name;
	{string} characters;
}



//Loading variables from data file
{string} CharacterTypes = ...; //Set of possible character types

{Character} Characters with type in CharacterTypes = ...; //Set of Characters

{string} LeadingCharacters = ...; //Set of leading characters

int maxNrOfCharacters = ...; //Maximum allowed number of characters per actor

{Scene} Scenes = ...; //Set of Scenes
int nrOfScenes = card(Scenes);

range ActorRange = 1..card(Characters);

//Decision variables
dvar int ActorToCharacter[Characters] in ActorRange;
dexpr int NrOfActors = max(c in Characters) ActorToCharacter[c];

execute {
	cp.param.Workers = 1;
  	cp.param.TimeLimit = 5; 
}

minimize
  NrOfActors;

subject to {
	// We want no actor that plays more than maxNrOfCharacters characters.
	forall(i in ActorRange) {
		count(ActorToCharacter, i) <= maxNrOfCharacters;	
	}
	// We want no characters with a different type played by the same actor.
	// This is equivalent to having men play only male roles, and women play only female roles.
	forall(c,d in Characters) {
		if (c.type != d.type) { // Note that if c.type != d.type then also c != d.
			ActorToCharacter[c] != ActorToCharacter[d];			
		}		
	}
	// We want no scenes where two characters have the same actor. Note that if more than two characters
	// are played by the same actor, also two characters are played by the same actor.
	// This is equivalent to having no actor play more than one character in any scene.
//	forall(s in Scenes) {
//		forall(c, d in Characters) {
//			if (c != d && c.name in s.characters && d.name in s.characters) {
//				ActorToCharacter[c] != ActorToCharacter[d];				
//			}					
//		}	
//	}
	forall(s in Scenes) {
		allDifferent(all(c in Characters : c.name in s.characters) ActorToCharacter[c]);	
	}
//	// We want no character that is a leading character to have the same actor as any other character.
	forall(c,d in Characters) {
		if (c != d && c.name in LeadingCharacters) {
			ActorToCharacter[c] != ActorToCharacter[d];
		}	
	}
	// Extra constraint that fixes the actor number of leading characters to the index of the leading character in LeadingCharacters.
	forall(c in Characters) {
		if (c.name in LeadingCharacters) {
			ActorToCharacter[c] == ord(LeadingCharacters, c.name) + 1;		
		}	
	}
	// We want no pair of consecutive scenes where there exists a character in the first scene and a character in the second scene
	// such that the characters are different but the actors are the same.
	// This is equivalent to no actor playing two different characters in two consecutive scenes.
//	forall(s,t in Scenes) {
//		forall(c, d in Characters) {	
//			if (s != t && (ord(Scenes, s) == (ord(Scenes, t) - 1)) && c != d && c.name in s.characters && d.name in t.characters) {
//				ActorToCharacter[c] != ActorToCharacter[d];								
//			}	
//		}	
//	}		
	// An actor cannot play two different characters in subsequent scenes
	forall(s in 0..nrOfScenes-2){
		forall(c, d in Characters){
			if(c != d && c.name in item(Scenes, s).characters && d.name in item(Scenes, s+1).characters){
				ActorToCharacter[c] != ActorToCharacter[d];
			}	
		}
	}	
}
//Amount of actors for each character type
int NrOfActorsNeeded = NrOfActors;  //For some reason, it doesn't work with NrOfActors inside the next loop (it is a dexpr?)
int nrOfActorsOfType[ct in CharacterTypes] = 
card({i | i in 0..NrOfActorsNeeded, c in Characters 
       : i == ActorToCharacter[c] && c.type == ct});

//Which characters are played by an actor
{Character} CharactersPlayedByActor[i in 1..NrOfActors] = 
	{c | c in Characters : i == ActorToCharacter[c]};
	
execute {  	
  	writeln("Actors needed: ", NrOfActors);
  	  	
  	for(var ct in CharacterTypes) {
  		writeln(ct, " needed: ", nrOfActorsOfType[ct]);
   	}  	  
   			     	
  	for(var i=1; i<=NrOfActors; i++) {
  	  writeln("Actor ", i, " plays ", CharactersPlayedByActor[i]);
    }  	  
}